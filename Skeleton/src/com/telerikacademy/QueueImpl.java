package com.telerikacademy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class QueueImpl<T> implements Queue<T> {
    private int head;
    private int tail;
    private T[] data;

    public QueueImpl() {
        this.head = -1;
        this.tail = -1;
        this.data = (T[]) new Object[10];
    }

    @Override
    public void offer(T elem) {
        if (isEmpty()) {
            head++;
        }
        tail++;
        if (tail == data.length) {
            resizeData();
        }
        data[tail] = elem;
    }

    @Override
    public T poll() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        T result = data[head];
        head++;
        return result;
    }

    @Override
    public boolean isEmpty() {
        return tail == -1 || head > tail;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new IllegalArgumentException();
        }
        T result = data[head];
        return result;
    }

    @Override
    public int size() {
        if (isEmpty()) {
            return 0;
        }
        return (tail - head) + 1;
    }

    private void resizeData() {
           data = (T[]) new Object[data.length*2];
    }
}

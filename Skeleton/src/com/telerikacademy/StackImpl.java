package com.telerikacademy;

public class StackImpl<T> implements Stack<T> {

    private int  count;

    private static class Node<T> {
        private T data;
        private Node next;


        private Node(T data) {
            this.data = data;
        }
    }

    private Node<T> topElement;

    @Override
    public void push(T elem) {
//        if (topElement == null) {
//            throw  new IllegalArgumentException();
//        }
        Node node = new Node(elem);
        node.next = topElement;
        topElement = node;
        count++;
    }

    @Override
    public T pop() {
        if (topElement == null) {
            throw  new IllegalArgumentException();
        }
        T result = topElement.data;
        topElement = topElement.next;
        count--;
        return result;
    }

    @Override
    public T peek() {
        if (topElement == null) {
           throw  new IllegalArgumentException();
        }
        return topElement.data;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return topElement == null;
    }
}



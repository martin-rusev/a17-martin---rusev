package com.telerikacademy.Tasks;

import java.util.Arrays;
import java.util.Scanner;

public class AnagramStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] firstString = scanner.nextLine().toCharArray();
        char[] secondString = scanner.nextLine().toCharArray();

        //Using Arrays sort method to sort the strings
        Arrays.sort(firstString);
        Arrays.sort(secondString);

        System.out.println(Arrays.equals(firstString, secondString));
        System.out.println(isAnagram(Arrays.toString(firstString), Arrays.toString(secondString)));

    }

    //Using recursion
    private static boolean isAnagram(String firstString, String secondString) {
        if (firstString.length() != secondString.length()) return false;
        if (firstString.isEmpty()) return true;

        char checker = firstString.charAt(0);
        int index = secondString.indexOf(Character.toString(checker));

        if (index == -1) return false;

        StringBuilder stringBuilder = new StringBuilder(secondString);
        stringBuilder.deleteCharAt(index);
        String newString = String.valueOf(stringBuilder);

        return isAnagram(firstString.substring(1),newString);
    }
}

package com.telerikacademy.Tasks;

public class BirdMountain {
    /* A point is 1 if for a given char[x][y]:
     * char[x+/-1][n] is whitespace
     * - char[x][n+/-1] is whitespace
     * - everything in the first and last row is a 1 by default;

     * A point is 2 if for a given char[x][y]:
     * - any char[x+-1][n] or char[x][n+-1] is a 1;
     *
     * A point is 3 if it is touching a 2. And so on.
     */
    public static int peakHeight(char[][] mountain) {

        int colSize = mountain[0].length;
        int rowSize = mountain.length;
        int[][] output = new int[rowSize][colSize];
        System.out.println(colSize);
        char current;
        int maxHeight = 0;


        // The maximum number of runs should be less than the larger of the two.
        // Worst case might be for whichever is smaller in size.
        for (int i = 0; i < 15; i++) {
            // For the first run we want to check for whitespaces to replace
            if (i == 0) {
                current = ' ';

                // All other runs must check for integer characters
            } else {
                current = (char) (i + '0');
            }

            // Main logic
            for (int x = 0; x < rowSize; x++) {
                for (int y = 0; y < colSize; y++) {

                    // Check for max height
                    if (output[x][y] > maxHeight) {
                        maxHeight = output[x][y];
                    }

                    // If you are in an inner row:
                    if (x != 0 && y != 0 && x < rowSize - 1 && y < colSize - 1 && mountain[x][y] == '^') {
                        if (mountain[x - 1][y] == current || mountain[x + 1][y] == current || mountain[x][y - 1] == current || mountain[x][y + 1] == current) {
                            if (current == ' ') {
                                mountain[x][y] = '1';
                                output[x][y] = 1;
                            } else {
                                mountain[x][y] = (char) (current + 1);
                                output[x][y] = i + 1;
                            }
                        }

                        // Else you are in an outer row:
                    } else {
                        if ((x == 0 || x == rowSize - 1 || y == 0 || y == colSize - 1) && mountain[x][y] == '^') {
                            mountain[x][y] = '1';
                            output[x][y] = 1;
                        }
                    }
                }
                System.out.println(output[x] + "");
            }


        }


        return maxHeight;
    }
}

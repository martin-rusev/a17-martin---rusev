package com.telerikacademy.Tasks;

public class BuddyPairs {
    public static String buddy(long start, long limit) {

        for (long i = start; i <= limit; i++) {
            long j = divSum(i) - 1;
            if (j <= i) {
                continue;
            }
            if (i == divSum(j) - 1) {
                return "(" + i + " " + j + ")";
            }
        }

        return "Nothing";
    }

    static long divSum(long num) {
        long sum = 1;

        for (long i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                sum += (i + (num / i));
            }
        }
        return sum;
    }
}

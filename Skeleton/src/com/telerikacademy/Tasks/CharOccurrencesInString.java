package com.telerikacademy.Tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CharOccurrencesInString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        char charInput = scanner.nextLine().charAt(0);
        System.out.println(occurrences(charInput, input));
        System.out.println(countOccurrences(input, charInput, 0));
    }

    //Simple solution with List and foreach loop
    private static int occurrences(char ch, String input) {
        int number = 0;
        List<Character> charList = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            charList.add(input.charAt(i));
        }

        for (char charr : charList) {
            if (charr == ch) {
                number++;
            }
        }
        return number;
    }

    //Solution with recursion
    private static int countOccurrences(String someString, char searchedChar, int index) {

        if (index >= someString.length()) {
            return 0;
        }

        int count = someString.charAt(index) == searchedChar ? 1 : 0;
        return count + countOccurrences(
                someString, searchedChar, index + 1);
    }
}

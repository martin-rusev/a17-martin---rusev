package com.telerikacademy.Tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CountUniqueElementsInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(countUniqueElements(input));

    }

    static int countUniqueElements(String input) {
        String[] str = input.split(" ");
        int[] array = new int[str.length];

        for (int i = 0; i < str.length; i++) {
            array[i] = Integer.parseInt(str[i]);
        }

        List<Integer> myList = new ArrayList<>();

        for (int num: array) {
            if (!myList.contains(num)) {
                myList.add(num);
            }
        }
        return myList.size();

    }
}

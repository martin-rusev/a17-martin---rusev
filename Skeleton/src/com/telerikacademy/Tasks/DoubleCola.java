package com.telerikacademy.Tasks;

public class DoubleCola {
    public static String WhoIsNext(String[] names, int n) {
        n = n - 1;
        int length = names.length;

        while (n >= length) {
            int div = n - length;
            n = (int) Math.floor(div / 2);
        }
        return names[n];
    }
}

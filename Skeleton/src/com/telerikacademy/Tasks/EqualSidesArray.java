package com.telerikacademy.Tasks;

public class EqualSidesArray {
    public static int findEvenIndex(int[] arr) {
        int sum = 0;
        int leftSum = 0;

        //Find the whole integers sum
        for (int num : arr) {
            sum += num;
        }

        //Subtracting the corresponding integer from the whole sum and if the sums are equal,
        //returning the index. Otherwise return -1.
        for (int i = 0; i < arr.length; i++) {
            sum -= arr[i];

            if (leftSum == sum) {
                return i;
            }

            leftSum += arr[i];
        }

        return -1;
    }
}

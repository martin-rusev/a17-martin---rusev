package com.telerikacademy.Tasks;

public class FibonacciNumbers {
    private static int fibonacci(int N) {
        if (N == 0) {
            return 0;
        }
        if (N == 1) {
            return 1;
        }

        return fibonacci(N - 1) + fibonacci(N - 2);
    }

    //print the exact fibonacci number
    public static void main(String[] args) {
        System.out.println(fibonacci(7));

        //print the whole fibonacci sequence
//        int index = 0;
//        while (true) {
//            System.out.println(fibonacci(index));
//        index++;
//        }

    }

}

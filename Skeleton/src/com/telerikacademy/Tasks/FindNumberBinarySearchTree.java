package com.telerikacademy.Tasks;

public class FindNumberBinarySearchTree {
    public Node search(Node root, int value) {
        // Base Cases: root is null or key is present at root
        if (root == null || root.value == value)
            return root;

        // val is greater than root's key
        if (root.value > value)
            return search(root.left, value);

        // val is less than root's key
        return search(root.right, value);
    }
}

package com.telerikacademy.Tasks;

import java.util.Scanner;

public class MergeStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String first = scanner.nextLine();
        String second = scanner.nextLine();
        System.out.println(mergeStrings(first, second));
    }

    private static String mergeStrings(String first, String second) {
        StringBuilder mergedString = new StringBuilder();
        int maxLength = Math.max(first.length(), second.length());
        for (int i = 0; i < maxLength; i++) {
            if (i < first.length()) {
                mergedString.append(first.charAt(i));
            }
            if (i < second.length()) {
                mergedString.append(second.charAt(i));
            }
        }
        return mergedString.toString();
    }
}

package com.telerikacademy.Tasks;

import jdk.nashorn.internal.runtime.regexp.RegExp;

import java.util.ArrayList;
import java.util.List;

public class NewCashier {

    public static String getOrder(String input) {

        //Holding all the products in a String array.
        String[] menu = {"Burger", "Fries", "Chicken", "Pizza",
                "Sandwich", "Onionrings", "Milkshake", "Coke"};

        //Create instance of StringBuilder to hold the result.
        StringBuilder sb = new StringBuilder();

        //Appending the product in the StringBuilder instance if the string input contains it
        // and then remove the product from the string input.
        // Return the result with whitespace removed.
        for (String product : menu) {
            while (input.contains(product.toLowerCase())) {
                sb.append(product).append(" ");
                input = input.replaceFirst(product.toLowerCase(), "");
            }
        }
        return sb.toString().trim();
    }
}

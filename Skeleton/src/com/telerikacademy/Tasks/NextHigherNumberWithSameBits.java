package com.telerikacademy.Tasks;

public class NextHigherNumberWithSameBits {

    static int nextHigher(int n) {

        int right;
        int nextHighBit;
        int rightPattern;
        int next = 0;

        if (n > 0) {

            right = n & -n;
            nextHighBit = n + right;
            rightPattern = n ^ nextHighBit;
            rightPattern = (rightPattern) / right;
            rightPattern >>= 2;
            next = nextHighBit | rightPattern;
        }

        return next;
    }
}

package com.telerikacademy.Tasks;

public class NumberZooPatrol {
    public static void main(String[] args) {
        int[] a = { 1, 2, 4, 5, 6 };
        int miss = findMissingNumber(a);
        System.out.println(miss);
    }


    public static int findMissingNumber(int[] numbers) {
        long i;
        long missedNumber;
        long length = numbers.length;

        missedNumber = (length + 1) * (length + 2) / 2;

        //Looping to figure it out the missedNumber. On each loop we subtract the number
        // from the number - current index in the array.
        for (i = 0; i < length; i++) {
            missedNumber -= numbers[(int)i];
        }
        //Making cast to integer because of the last test case
        // and the requirement to return integer.
        return (int)missedNumber;
    }
}

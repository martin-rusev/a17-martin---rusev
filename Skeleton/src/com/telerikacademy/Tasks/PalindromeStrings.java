package com.telerikacademy.Tasks;

import java.util.Scanner;

public class PalindromeStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        //Using StringBuilder instance
        StringBuilder stringBuilder = new StringBuilder(input);
        stringBuilder.reverse();
        String reversedString = String.valueOf(stringBuilder);
        boolean isPalindrome = input.equals(reversedString);
        System.out.println(isPalindrome);

        //Using Loop
        String reversed = "";
        for (int i = input.length() - 1; i >= 0; i--) {
            reversed = reversed + input.charAt(i);
        }
        System.out.println(input.equals(reversed));

        //Using Recursion
        System.out.println(isPal(input));

    }

    public static boolean isPal(String checkedString) {
        if (checkedString.length() == 0 || checkedString.length() == 1)
            return true;
        if (checkedString.charAt(0) == checkedString.charAt(checkedString.length() - 1))
            /* check for first and last char of String:
             * if they are same then do the same thing for a substring
             * with first and last char removed. and carry on this
             * until you string completes or condition fails
             * Function calling itself: Recursion
             */
            return isPal(checkedString.substring(1, checkedString.length() - 1));

        return false;
    }

    //Using Stack
//    Stack stack = new Stack();
//
//        for (int i = 0; i < inputString.length(); i++) {
//        stack.push(inputString.charAt(i));
//    }
//
//    String reverseString = "";
//
//        while (!stack.isEmpty()) {
//        reverseString = reverseString+stack.pop();
//    }
//
//        if (inputString.equals(reverseString))
//            System.out.println("The input String is a palindrome.");
//        else
//                System.out.println("The input String is not a palindrome.");

}

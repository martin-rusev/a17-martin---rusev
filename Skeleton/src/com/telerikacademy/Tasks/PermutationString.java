package com.telerikacademy.Tasks;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class PermutationString {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(getPermutations(input));
        permutations("", input);
    }

    public static void permutations(String permutation, String input) {
        if (input.length() == 0) {
            System.out.println(permutation);
        }

        for (int i = 0; i < input.length(); i++) {
            String newPermutation = permutation + input.charAt(i);
            String newRemainingString = input.substring(0, i) +
                    input.substring(i + 1);
            permutations(newPermutation, newRemainingString);
        }
    }

    //Recursion using Set

    public static Set<String> getPermutations(String str) {

        // Create a set to avoid duplicate permutation
        Set<String> permutations = new HashSet<>();

        // Terminating condition for recursion
        if (str.length() == 0) {
            permutations.add("");
            return permutations;
        }

        // Get the first character
        char first = str.charAt(0);

        // Get the remaining substring
        String sub = str.substring(1);

        // Make recursive call to getPermutation()
        Set<String> words = getPermutations(sub);

        // Access each element from words
        for (String strNew : words) {
            for (int i = 0; i <= strNew.length(); i++) {

                // Insert the permutation to the set
                permutations.add(strNew.substring(0, i) + first + strNew.substring(i));
            }
        }
        return permutations;
    }
}

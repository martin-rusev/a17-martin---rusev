package com.telerikacademy.Tasks;

public class Recursion101 {
    static int[] solve(int a, int b) {

        while (a >= (2 * b) || b >= (2 * a)) {
            if (a == 0 || b == 0) {
                return new int[]{a,b};
            }
            if (a >= (2 * b)) {
                a = a % (2 * b);
            } else {
                b = b % (2 * a);
            }
        }
        return new int[]{a,b};
    }
}




package com.telerikacademy.Tasks;

import java.util.Arrays;
import java.util.Scanner;

public class ReverseString {
    public static void main(String[] args) {

        //Using Recursion
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(reverseString(input));

        //Using StringBuilder
        StringBuilder stringBuilder = new StringBuilder(input);
        System.out.println(stringBuilder.reverse());

        //Using Loop
        for (int i = input.length() - 1; i >= 0  ; i--) {
            System.out.print(input.charAt(i));
        }
    }

    private static String reverseString(String input) {
        //reversed(Abv)
        // reversed(bv) + A -> vb + A = vbA
        // reversed(v) + b -> v+b= vb

        if (input.length() <= 1) {
            return input;
        }
        return reverseString(input.substring(1)) + input.substring(0,1);
    }

}

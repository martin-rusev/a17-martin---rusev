package com.telerikacademy.Tasks;

import java.util.ArrayList;
import java.util.List;

public class Snail {
    public static int[] snail(int[][] array) {
        List<Integer> list= new ArrayList<Integer>();

        int k=0,l=0,i;
        int m=array.length;
        int n=array[0].length;
        while (k < m && l < n) {
            // Print the first row from the remaining rows
            for (i = l; i < n; ++i) {
                list.add(array[k][i]);
            }
            k++;

            // Print the last column from the remaining columns
            for (i = k; i < m; ++i) {
                list.add(array[i][n - 1]);
            }
            n--;

            // Print the last row from the remaining rows */
            if (k < m) {
                for (i = n - 1; i >= l; --i) {
                    list.add(array[m - 1][i]);
                }
                m--;
            }

            // Print the first column from the remaining columns */
            if (l < n) {
                for (i = m - 1; i >= k; --i) {
                    list.add(array[i][l]);
                }
                l++;
            }

        }
        int[] arr= new int[list.size()];
        for(i=0;i<list.size();i++)
            arr[i]=list.get(i);
        return arr;
        // enjoy
    }
}

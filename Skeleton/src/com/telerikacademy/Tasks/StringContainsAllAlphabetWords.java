package com.telerikacademy.Tasks;

import java.util.Scanner;

public class StringContainsAllAlphabetWords {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(isStringHasAllWords(input));
        System.out.println(isStringHasAllWordsRegex(input));

    }

    //Using Streaming API
    static boolean isStringHasAllWords(String input) {
        return input.chars().filter(i -> i >= 'a' && i <= 'z').distinct().count() == 26;
    }

    static boolean isStringHasAllWordsRegex(String input) {
        //Replace everything that is not between a-zA-Z
        input = input.replaceAll("[^a-zA-Z]", "");

        input = input.toLowerCase();

        //Replace duplicate characters
        input = input.replaceAll("(.)(?=.*\\1)", "");
        System.out.println(input);
        return input.length() == 26;

    }
}

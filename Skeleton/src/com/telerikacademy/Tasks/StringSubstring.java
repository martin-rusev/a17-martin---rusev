package com.telerikacademy.Tasks;

import java.util.Scanner;

public class StringSubstring {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String substring = scanner.nextLine();
        String checkedString = "HelloHelloHello!";

        //Using contains() method
        boolean isSubstring = checkedString.contains(substring);
        System.out.println("The string is substring: " + isSubstring);

        //Using indexOf() method
        boolean isFound = checkedString.indexOf(substring) != -1 ? true : false;
        System.out.println("The string is substring: " + isFound);

        System.out.println(occurrences(checkedString, substring));
    }

    //Using recursion to count the occurrences of a substring
    static int occurrences(String checkedString, String substring) {

        if (checkedString.contains(substring)) {
            return 1 + occurrences(checkedString.replaceFirst(substring, ""), substring);
        }
        return 0;
    }


}


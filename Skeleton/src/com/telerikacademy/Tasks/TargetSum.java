package com.telerikacademy.Tasks;

import java.util.HashMap;
import java.util.Map;

public class TargetSum {
    public int findTargetSumWays(int[] nums, int s) {
        if (nums == null || nums.length == 0)
            return 0;
        Map<String, Integer> cache = new HashMap<>();
        return compute(cache, nums, 0, s);
    }

    private int compute(Map<String, Integer> cache, int[] nums, int pos, int target) {
        if (pos == nums.length)
            return target == 0 ? 1 : 0;

        String key = pos + "/" + target;
        if (cache.containsKey(key))
            return cache.get(key);

        int ways = compute(cache, nums, pos + 1, target + nums[pos])
                + compute(cache, nums, pos + 1, target - nums[pos]);

        cache.put(key, ways);
        return ways;
    }
}

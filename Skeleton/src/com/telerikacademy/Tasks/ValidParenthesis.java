package com.telerikacademy.Tasks;

import com.telerikacademy.Stack;
import com.telerikacademy.StackImpl;

import java.util.Scanner;

public class ValidParenthesis {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String expr = scanner.nextLine();

        Stack<Character> stack = new StackImpl<>();

        boolean areBalanced = true;
        for(int i = 0; i < expr.length(); i++){
            char openBracket = expr.charAt(i);

            if('(' == openBracket || '[' == openBracket || '{' == openBracket){
                stack.push(openBracket);
            }else {
                if(stack.isEmpty()){
                    areBalanced = false;
                    break;
                }
                openBracket = stack.pop();
                char closeBracket = expr.charAt(i);
                if(openBracket == '(' && closeBracket != ')'){
                    areBalanced = false;
                    break;
                }else if(openBracket == '{' && closeBracket != '}'){
                    areBalanced = false;
                    break;
                }else if(openBracket == '[' && closeBracket != ']'){
                    areBalanced = false;
                    break;
                }
            }
        }
        System.out.println(areBalanced);
    }
}
